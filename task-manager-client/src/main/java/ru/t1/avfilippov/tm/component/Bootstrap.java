package ru.t1.avfilippov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.api.service.ILoggerService;
import ru.t1.avfilippov.tm.event.ConsoleEvent;
import ru.t1.avfilippov.tm.listener.AbstractListener;
import ru.t1.avfilippov.tm.util.SystemUtil;
import ru.t1.avfilippov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.avfilippov.tm.command";

    @NotNull
    @Autowired
    private AbstractListener[] abstractCommands;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    public void run(final String[] args) {
        processCommands();
    }

    private String readCommand() {
        return TerminalUtil.nextLine();
    }

    public void processCommands() {
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull String command = readCommand();
                if (command.isEmpty()) {
                    continue;
                }
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void prepareStartup() {
        try {
            initPID();
            Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
            loggerService.info("*** WELCOME TO TASK MANAGER CLIENT ***");
        } catch (@NotNull final Exception e) {
            loggerService.info("[INIT FAIL]");

        }
    }

    private void prepareShutdown() {
        loggerService.info("*** TASK MANAGER IS SHUTTING DOWN***");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
