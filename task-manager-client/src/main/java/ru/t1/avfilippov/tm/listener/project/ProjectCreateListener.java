package ru.t1.avfilippov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.ProjectCreateRequest;
import ru.t1.avfilippov.tm.event.ConsoleEvent;
import ru.t1.avfilippov.tm.util.TerminalUtil;

@Component
public final class ProjectCreateListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getDescription() {
        return "create new project";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    @EventListener(condition = "@projectCreateListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        projectEndpoint.createProject(request);
    }

}
