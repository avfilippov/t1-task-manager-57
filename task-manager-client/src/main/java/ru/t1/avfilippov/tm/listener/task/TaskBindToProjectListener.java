package ru.t1.avfilippov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.TaskBindToProjectRequest;
import ru.t1.avfilippov.tm.event.ConsoleEvent;
import ru.t1.avfilippov.tm.util.TerminalUtil;

@Component
public final class TaskBindToProjectListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getDescription() {
        return "bind task to project";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-bind-to-project";
    }

    @Override
    @EventListener(condition = "@taskBindToProjectListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        taskEndpoint.bindTaskToProject(request);
    }

}
