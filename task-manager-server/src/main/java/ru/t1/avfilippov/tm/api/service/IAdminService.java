package ru.t1.avfilippov.tm.api.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAdminService {

    void dropScheme(@Nullable String initToken);

    void initScheme(@Nullable String initToken);

    @NotNull
    Liquibase getLiquibase();

}
