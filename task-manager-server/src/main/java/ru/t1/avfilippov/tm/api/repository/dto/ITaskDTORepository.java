package ru.t1.avfilippov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTORepository extends IUserOwnedDTORepository<TaskDTO> {

    @NotNull
    TaskDTO create(@NotNull String userId,
                   @NotNull String name,
                   @NotNull String description);

    @NotNull
    TaskDTO create(@NotNull String userId, @NotNull String name);

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}