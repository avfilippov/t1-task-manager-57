package ru.t1.avfilippov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.avfilippov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;

@Repository
@Scope("prototype")
public final class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

    @Override
    @NotNull
    protected Class<SessionDTO> getClazz() {
        return SessionDTO.class;
    }

}
