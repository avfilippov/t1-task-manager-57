package ru.t1.avfilippov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.avfilippov.tm.api.repository.model.ISessionRepository;
import ru.t1.avfilippov.tm.model.Session;

@Repository
@Scope("prototype")
public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @Override
    @NotNull
    protected Class<Session> getClazz() {
        return Session.class;
    }

}
