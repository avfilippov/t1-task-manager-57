package ru.t1.avfilippov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.dto.request.SchemeDropRequest;
import ru.t1.avfilippov.tm.dto.request.SchemeInitRequest;
import ru.t1.avfilippov.tm.dto.request.ServerAboutRequest;
import ru.t1.avfilippov.tm.dto.request.ServerVersionRequest;
import ru.t1.avfilippov.tm.dto.response.SchemeDropResponse;
import ru.t1.avfilippov.tm.dto.response.SchemeInitResponse;
import ru.t1.avfilippov.tm.dto.response.ServerAboutResponse;
import ru.t1.avfilippov.tm.dto.response.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAdminEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AdminEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAdminEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }


    @SneakyThrows
    @WebMethod(exclude = true)
    static IAdminEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IAdminEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAdminEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IAdminEndpoint.class);
    }


    @NotNull
    @WebMethod
    SchemeInitResponse initScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull SchemeInitRequest request
    );

    @NotNull
    @WebMethod
    SchemeDropResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull SchemeDropRequest request
    );

}
